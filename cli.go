package oktaws

import (
	"flag"
	"fmt"
	"io"
	"os"
)

// Parse is the standard command-line parsing function.
func Parse(
	in io.Reader,
	out, err io.Writer,
	env func(string) (string, bool),
	exit func(int),
	args ...string,
) {
	var version, help bool
	fs := flag.NewFlagSet(Name, flag.ExitOnError)
	fs.BoolVar(&help, "h", false, "print help")
	fs.BoolVar(&version, "V", false, "print the version")
	fs.Parse(args)

	if help {
		fs.SetOutput(out)
		fs.Usage()
		fmt.Fprintln(fs.Output(), Description)
		return
	}

	if version {
		fmt.Fprintf(out, "%s v%s\n", Name, Version)
		return
	}

}

// A CLI helps building command-line interfaces
type CLI struct {
	// ConfigFile is the location of the configuration file (containing profiles
	// and options)
	ConfigFile string
	// OktaUsername is the username used to log into Okta
	Username string
	// OktaPassword is a function which returns a password to log into Okta
	Password func(string) string
	// OktaMFACode is a function which returns an MFA code that can be used
	// for 2-factor authentication with Okta
	MFACode func(string) string
	// ProfileName is the name of a profile
	ProfileName string
	// RoleARN is an AWS role ARN
	RoleARN string
}

// A Parser can parse command-line flags and return a function to be run
type Parser struct {
	// In is the input  stream
	In io.Reader
	// Out is the output stream
	Out io.Writer
	// Err is the error stream
	Err io.Writer
	// Exit is a function called when we want to quit the process
	Exit func(int)
	// Env is used to retrieve environment variables, returning a value and
	// whether it was found.
	Env func(string) (string, bool)
}

// Parse process the given command-line arguments and returns an action. (An
// action is a function which returns an exit code.)
func (p *Parser) Parse(args []string) {}

// Name of the programme
const Name = "oktaws"

// Version number
const Version = "0.0.1"

// Description of the behaviour
const Description = `
Gets an AWS session token (and access-key ID and secret access-key) suitable
for use by the 'credentials_process' in an AWS configuration.

To use this tool, create a profile in your AWS config file with

	credential_process=sh -c oktaws -p PROFILE 2>/dev/tty

(replace PROFILE with the name of the profile you configure, or leave out the
option to use a profile named 'default').

Profiles are stored (by default) as PROFILE.json in $XDG_CONFIG/oktaws (or
$HOME/.config/oktaws) and data is cached in $XDG_CACHE/oktaws (or
$HOME/.cache/oktaws); these directories are created as needed.

The password and the MFA code are _not_ stored in the profile, but obtained
via a command which is specified in the profile instead. These commamds must
exit non-zero and output the password/code to STDOUT. For example, given
~/.config/oktaws/myProfile.json containing:

{
  "url: "https://my-example-app.okta.com/abcded/123",
	"username": "alice@example.com",
	"role": "arn:aws:iam::123456789012:role/myRole",
	"password": "pass mypassword",
	"mfa": "pass otp mymfa"
}
`

// run parses the args
var run = func(args []string, stdout, stderr io.Writer, exit func(int)) {
	var help, version, verbose bool
	var profile, role, url, user, pass, mfa, cfg string

	flag := flag.NewFlagSet("oktaws", flag.ExitOnError)
	flag.BoolVar(&help, "h", false, "show this message")
	flag.BoolVar(&version, "V", false, "show the version number")
	flag.BoolVar(&verbose, "v", false, "verbose output")
	flag.StringVar(&cfg, "f", getenv("OKTAWS_CFG", ""), "Config file")
	flag.StringVar(&profile, "p", getenv("OKTAWS_PROFILE", ""), "Profile name")
	flag.StringVar(&role, "r", getenv("OKTAWS_ROLE", ""), "AWS role ARN")
	flag.StringVar(&url, "u", getenv("OKTAWS_URL", ""), "IDP URL")
	flag.StringVar(&user, "U", getenv("OKTAWS_USER", ""), "Okta username")
	flag.StringVar(&pass, "P", getenv("OKTAWS_PASS", ""), "Okta password")
	flag.StringVar(&mfa, "M", getenv("OKTAWS_MFA", ""), "Okta MFA secret")

	flag.SetOutput(stderr)
	flag.Parse(args)

	if help {
		flag.SetOutput(stdout)
		flag.Usage()
		fmt.Fprintf(stdout, `
A small helper to provide an AWS session using Okta as an IDP provider.
	`)
		exit(0)
		return
	}

	if version {
		fmt.Fprintf(stdout, "%s v%s\n", Name, Version)
		exit(0)
		return
	}

	if len(flag.Args()) < 1 {
		flag.SetOutput(stderr)
		flag.Usage()
		exit(1)
		return
	}

	switch flag.Args()[0] {
	// case "add":
	// 	add(flag.Args()[1:])
	// case "list":
	// 	list(flag.Args()[1:])
	// case "rm":
	// 	rm(flag.Args()[1:])
	default:
		profile := flag.Args()[0]
		fmt.Fprintf(stdout, "using profile %s\n", profile)
	}
}

// getenv returns the environment variable, or the fallback
func getenv(key, fallback string) string {
	if v, found := os.LookupEnv(key); found {
		return v
	}
	return fallback
}
